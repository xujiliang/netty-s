package com.narata.netty.util;

import java.nio.ByteBuffer;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:45 2022-04-24
 * @Modified By:
 **/
public class BufferUtil {

    public static void print(ByteBuffer byteBuffer) {
        while (byteBuffer.hasRemaining()) { // 是否还有剩余的未读数据
            byte b = byteBuffer.get();
            System.out.print((char) b);
        }

        byteBuffer.clear();
    }
}
