package com.narata.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:59 2022-06-07
 * @Modified By:
 **/
public class TestConnectionTimeout {

    public static void main(String[] args) {
        // 1 客户端通过.option() 方法配置参数给SocketChannel配置参数


        // 2 服务端
//        new ServerBootstrap().option(); 是给ServerSocketChannel配置参数
//        new ServerBootstrap().childOption(); 给SocketChannel配置参数
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {

            Bootstrap bootstrap = new Bootstrap()
                    .group(group)
                    // 配置超时参数
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS,1000)
                    .channel(NioSocketChannel.class)
                    .handler(new LoggingHandler());

            ChannelFuture future = bootstrap.connect("127.0.0.1", 8080);
            future.sync().channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
