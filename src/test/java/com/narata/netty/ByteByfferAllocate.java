package com.narata.netty;

import java.nio.ByteBuffer;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:03 2021-10-29
 * @Modified By:
 **/
public class ByteByfferAllocate {
    public static void main(String[] args) {
        /**
         * java.nio.HeapByteBuffer 堆内存，读写效率低，会受到垃圾回收的影响
         *
         * java.nio.DirectByteBuffer 直接内存， 读写效率高（少一次拷贝），不会受到垃圾回收影响，
         * 分配内存效率低下
         */
        System.out.println(ByteBuffer.allocate(16).getClass()); // java.nio.HeapByteBuffer

        System.out.println(ByteBuffer.allocateDirect(16).getClass()); // java.nio.DirectByteBuffer

        ByteBuffer buffer = ByteBuffer.allocate(16);

    }
}
