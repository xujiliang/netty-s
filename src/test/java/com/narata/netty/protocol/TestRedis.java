package com.narata.netty.protocol;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:29 2022-05-09
 * @Modified By:
 **/
@Slf4j
public class TestRedis {
    private static final byte[] LINE = {13, 10};
    public static void main(String[] args) {

        NioEventLoopGroup worker = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(worker);
        bootstrap.channel(NioSocketChannel.class);
        try {
            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));
                    ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                        @Override
                        public void channelActive(ChannelHandlerContext ctx) throws Exception {
                            ByteBuf byteBuf = sendRedisCommand(ctx, "set", "name", "tom");
                            ctx.writeAndFlush(byteBuf);

                        }
                    });
                }

                @Override
                public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                    ByteBuf byteBuf = (ByteBuf) msg;
                    System.out.println(byteBuf.toString(Charset.defaultCharset()));
                }
            });
            ChannelFuture future = bootstrap.connect("localhost",6379).sync();
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            log.error("client error:{}", e);
        } finally {
            worker.shutdownGracefully();
        }


    }

    private static ByteBuf sendRedisCommand(ChannelHandlerContext ctx,String command, String key, String value) {
        ByteBuf buffer = ctx.alloc().buffer();
        buffer.writeBytes("*3".getBytes());
        buffer.writeBytes(LINE);
        String s1 = "$" + command.length();
        buffer.writeBytes( s1.getBytes());
        buffer.writeBytes(LINE);
        buffer.writeBytes(command.getBytes());
        buffer.writeBytes(LINE);
        String s2 = "$" + key.length();
        buffer.writeBytes(s2.getBytes());
        buffer.writeBytes(LINE);
        buffer.writeBytes(key.getBytes());
        buffer.writeBytes(LINE);
        String s3 = "$" + value.length();
        buffer.writeBytes(s3.getBytes());
        buffer.writeBytes(LINE);
        buffer.writeBytes(value.getBytes());
        buffer.writeBytes(LINE);
       return buffer;
    }
}
