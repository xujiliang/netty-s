package com.narata.netty.multiThread;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:09 2022-04-24
 * @Modified By:
 **/
public class WriteClient {

    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();

        sc.connect(new InetSocketAddress("localhost", 8080));

        // 3 接收数据
        sc.write(Charset.defaultCharset().encode("1233445566778"));
        int count = 0;

    }
}
