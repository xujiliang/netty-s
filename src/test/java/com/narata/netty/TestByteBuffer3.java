package com.narata.netty;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 16:29 2021-10-31
 * @Modified By:
 **/
public class TestByteBuffer3 {

    public static void main(String[] args) {

        // 1 字符串转bytefuffer  bytebuffer任处于写模式
        ByteBuffer byteBuffer = ByteBuffer.allocate(10);
        byteBuffer.put("hello".getBytes());

        // 2  charset ByteBuffer处于读模式
        ByteBuffer byteBuffer1 = StandardCharsets.UTF_8.encode("hello");

        // 3 Wrap ,进入读模式，与2相同
        ByteBuffer byteBuffer2 = ByteBuffer.wrap("hello".getBytes());

        String string = StandardCharsets.UTF_8.decode(byteBuffer1).toString();
        System.out.println(string);


        byteBuffer.flip();
        String string1 = StandardCharsets.UTF_8.decode(byteBuffer).toString();
        System.out.println(string1);
    }
}
