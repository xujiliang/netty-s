package com.narata.netty;

import javax.imageio.stream.FileImageInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class TestByteBuffer {

    public static void main(String[] args) {
        // FileChannel
        // 1 输入输出流 2 RandomAccessFile
        try {
            FileChannel channel = new FileInputStream("data.txt").getChannel();

            // 准备缓冲区,划分一块内存11个字节
            ByteBuffer byteBuffer = ByteBuffer.allocate(11);

            // 从channel读取数据，写入byteBuffer
            channel.read(byteBuffer);

            // 打印buffer内容
            byteBuffer.flip(); // 切换至读模式
            while (byteBuffer.hasRemaining()) { // 是否还有剩余的未读数据
                byte b = byteBuffer.get(); //get()方法一次读一个字节
                System.out.println((char) b);
            }

            byteBuffer.clear();// 切换为写模式
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
