package com.narata.netty;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:36 2021-10-31
 * @Modified By:
 **/
public class ScatteringWriteTest {

    public static void main(String[] args) {

        try {
            FileChannel channel = new RandomAccessFile("words.txt", "r").getChannel();
            ByteBuffer byteBuffer1 = ByteBuffer.allocate(3);
            ByteBuffer byteBuffer2 = ByteBuffer.allocate(3);
            ByteBuffer byteBuffer3 = ByteBuffer.allocate(5);
            channel.read(new ByteBuffer[]{byteBuffer1,byteBuffer2,byteBuffer3});

            byteBuffer1.flip();
            byteBuffer2.flip();
            byteBuffer3.flip();

            System.out.println(StandardCharsets.UTF_8.decode(byteBuffer1).toString());
            System.out.println(StandardCharsets.UTF_8.decode(byteBuffer2).toString());
            System.out.println(StandardCharsets.UTF_8.decode(byteBuffer3).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
