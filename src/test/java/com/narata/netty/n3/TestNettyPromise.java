package com.narata.netty.n3;

import io.netty.channel.EventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.DefaultPromise;

import java.util.concurrent.ExecutionException;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:31 2022-05-04
 * @Modified By:
 **/
public class TestNettyPromise {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 1 准备EventLoop对象
        EventLoop eventLoop = new NioEventLoopGroup().next();
        // 2 可以主动创建promise 结果容器
        DefaultPromise<Integer> promise = new DefaultPromise<>(eventLoop);
        new Thread(() -> {
            System.out.println("开始计算");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                promise.setFailure(e);
            }
            promise.setSuccess(100);

        }).start();

        // 4 接收结果
        System.out.println(promise.get());

    }
}
