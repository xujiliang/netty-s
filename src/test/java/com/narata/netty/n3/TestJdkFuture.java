package com.narata.netty.n3;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:37 2022-05-04
 * @Modified By:
 **/
@Slf4j
public class TestJdkFuture {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService service = Executors.newFixedThreadPool(2);

        Future<Integer> future = service.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                log.debug("执行计算");
                Thread.sleep(1000);
                return 50;
            }
        });

        log.debug("等待执行结果");

        System.out.println(future.get());
    }
}
