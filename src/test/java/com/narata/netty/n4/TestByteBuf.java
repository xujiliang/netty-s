package com.narata.netty.n4;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:35 2022-05-06
 * @Modified By:
 **/
public class TestByteBuf {

    public static void main(String[] args) {
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();

        // PooledUnsafeDirectByteBuf(ridx: 0, widx: 0, cap: 256)
        System.out.println(buf);

        StringBuilder sb = new StringBuilder();
        for (int i= 0; i< 300; i++) {
            sb.append("a");
        }

        //PooledUnsafeDirectByteBuf(ridx: 0, widx: 300, cap: 512)
        buf.writeBytes(sb.toString().getBytes());
        System.out.println(buf);
    }
}
