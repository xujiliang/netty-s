package com.narata.netty;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@Slf4j
public class TestByteBuffer2 {
    public static void main(String[] args) {
        // FileChannel
        // 1 输入输出流 2 RandomAccessFile
        try {
            FileChannel channel = new FileInputStream("data.txt").getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(11);

            while (true) {
                // 准备缓冲区

                // 从channel读取数据，写入buffer
                int len = channel.read(byteBuffer);

                if (len == -1) {  // -1表示没有内容了
                    break;
                }

                // 打印buffer内容
                byteBuffer.flip(); // 切换至读模式
                while (byteBuffer.hasRemaining()) { // 是否还有剩余的未读数据
                    byte b = byteBuffer.get();
                    System.out.println((char) b);
                }

                byteBuffer.clear(); // 切换到写模式
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
