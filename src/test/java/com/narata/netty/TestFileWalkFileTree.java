package com.narata.netty;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:01 2021-11-14
 * @Modified By:
 **/
public class TestFileWalkFileTree {

//    public static void main(String[] args) throws IOException {
//        final AtomicInteger dirCount = new AtomicInteger();
//        final AtomicInteger fileCount = new AtomicInteger();
//        Files.walkFileTree(Paths.get("C:\\apache-tomcat-8.0.45\\logs"),new SimpleFileVisitor<Path>() {
//            @Override
//            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
//                System.out.println("===============>" + dir);
//                dirCount.incrementAndGet();
//                return super.preVisitDirectory(dir, attrs);
//            }
//
//            @Override
//            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
//                System.out.println(file);
//                fileCount.incrementAndGet();
//                return super.visitFile(file, attrs);
//            }
//        });
//
//        System.out.println("dir count:" + dirCount);
//        System.out.println("file count:" + fileCount);
//    }

    public static void main(String[] args) throws IOException {
        // 多文件夹删除操作
        //Files.delete(Paths.get("C:\\apache-tomcat-8.0.45\\logs"));

        Files.walkFileTree(Paths.get("C:\\apache-tomcat-8.0.45\\logs"), new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

                Files.delete(dir);
                return super.postVisitDirectory(dir, exc);
            }
        });
    }

}
