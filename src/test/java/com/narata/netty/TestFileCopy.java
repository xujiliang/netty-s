package com.narata.netty;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.file.Files.walk;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:37 2021-11-15
 * @Modified By:
 **/
public class TestFileCopy {

    public static void main(String[] args) throws IOException {
        String source = "D:\\hadoop-2.7.2\\bin";

        String target = "D:\\hadoop-2.7.2_copy";


        walk(Paths.get(source)).forEach( path -> {

           try {
               String targetName = path.toString().replace(source, target);
               // 是目录
               if (Files.isDirectory(path)) {
                   Files.createDirectory(Paths.get(targetName));
               }

               // 是文件
               else if(Files.isRegularFile(path)) {
                    Files.copy(path, Paths.get(targetName));
               }
           }catch (Exception e) {

           }
        });
    }
}
