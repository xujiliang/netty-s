package com.narata.netty.c5;

import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:09 2022-04-24
 * @Modified By:
 **/
public class WriteClient {

    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();

        sc.connect(new InetSocketAddress("localhost", 8080));

        int count = 0;
        // 3 接收数据
        while (true) {
            ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024);

            count += sc.read(buffer);
            System.out.println(count);

            buffer.clear();
        }
    }
}
