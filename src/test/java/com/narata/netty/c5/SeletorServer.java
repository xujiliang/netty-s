package com.narata.netty.c5;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:55 2021-11-21
 * @Modified By:
 **/
@Slf4j
public class SeletorServer {

    public static void main(String[] args) throws IOException {

        // 1 创建一个Selector ，管理多个channel
        Selector selector = Selector.open();

        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);

        // 2 建立selector 和channel的联系
        // SelectionKey 就是将来事件发生后，通过他可以知道事件和那个channel发生的事件
        /**
         * 事件类型：
         * accept 会在有连接请求时触发
         * connect 是客户端，连接请求建立后触发
         * read 可读事件
         * write 可写事件
         */
        // 只关注accept事件
        ssc.register(selector, 0, null).interestOps(SelectionKey.OP_ACCEPT);

       // sscKey.interestOps(SelectionKey.OP_ACCEPT);
        // log.info("register key:{}", sscKey);

        ssc.bind(new InetSocketAddress(8080));

        while (true) {
            // 3 select方法，没有事件发生，阻塞，有事件，线程才会恢复运行
            // selector 在事件未处理时，他不会阻塞,事件发生后要么处理，要么取消
            selector.select();

            // 4 处理事件,返回所有发生事件
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                log.info("key:{}", key);
                // 5 区分事件类型
                if (key.isAcceptable()) { // 如果是accept事件
                    ServerSocketChannel channel = (ServerSocketChannel) key.channel();

                    SocketChannel sc = channel.accept();

                    sc.configureBlocking(false);
                    // Selector 管理SocketChannel
                    ByteBuffer buffer = ByteBuffer.allocate(10);
                    // 将ByteBuffer作为一个附件关联到selectionKey上
                    SelectionKey scKey = sc.register(selector, 0, buffer);
                    scKey.interestOps(SelectionKey.OP_READ);
                    log.info("{}",sc);
                } else if (key.isReadable()) { // 如果是read事件
                    try {

                        SocketChannel channel = (SocketChannel) key.channel(); // 拿到触发事件的channel
                        // 获取SelectionKey上关联的附件
                        ByteBuffer buffer = (ByteBuffer) key.attachment();
                        int read = channel.read(buffer);
                        if (read == -1) {
                            key.cancel();
                        } else {
                            split(buffer);
                            if (buffer.position() == buffer.limit()) {
                                ByteBuffer newBuffer = ByteBuffer.allocate(buffer.capacity() * 2);
                                buffer.flip();
                                newBuffer.put(buffer);
                                key.attach(newBuffer);
                            }
                            buffer.flip();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        key.cancel();
                    }
                }


            }
        }

    }

    private static void split(ByteBuffer source) {
        source.flip();
        for (int i=0;i<source.limit();i++) {
            byte b = source.get(i);
            // 找到一条完整的消息
            if (b == '\n') {
                int leng = i + 1 - source.position();
                // 把这条完整消息存入新的ByteBuffer
                ByteBuffer target = ByteBuffer.allocate(leng);
                // 从source写，向target读
                for (int j=0; j<leng;j++) {
                    target.put(source.get());
                }
            }
        }
        source.compact();
    }
}
