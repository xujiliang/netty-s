package com.narata.netty;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * @Author: XJL
 * @Description:  字符串转ByteBuffer
 * @Date: Create in 22:27 2021-11-07
 * @Modified By:
 **/
public class ByteBufferStringTest {

    public static void main(String[] args) {

        /**
         *
         * 字符串转ByteBuffer
         *
         */
        // 1 字符串转为ByteBuffer
        ByteBuffer buffer1 = ByteBuffer.allocate(16);
        buffer1.put("hello".getBytes());


        // 2 Charset
        ByteBuffer buffer2 = StandardCharsets.UTF_8.encode("hello");

        // 3 Wrap
        ByteBuffer buffer3 = ByteBuffer.wrap("hello".getBytes());


        /**
         * ByteBuffer 转字符串
         */

        String str1 = StandardCharsets.UTF_8.decode(buffer2).toString();

        buffer1.flip();
        String str2 = StandardCharsets.UTF_8.decode(buffer1).toString();

    }
}
