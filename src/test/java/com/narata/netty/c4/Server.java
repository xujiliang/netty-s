package com.narata.netty.c4;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:33 2021-11-16
 * @Modified By:
 **/
@Slf4j
public class Server {

    public static void main(String[] args) throws IOException {
        // 使用nio 来理解阻塞模式，单线程

        ByteBuffer buffer = ByteBuffer.allocate(16);

        // 1. 创建服务器
        ServerSocketChannel ssc = ServerSocketChannel.open();

        ssc.configureBlocking(false); //  socket切换为非阻塞模式 ， ssc.accept()方法会继续运行，如果没有连接建立，但是sc为null

        // 2绑定监听端口
        ssc.bind(new InetSocketAddress(8080));

        List<SocketChannel> channels = new ArrayList<>();
        while (true) {
            // 3.accept 建立与客户端的链接, SocketChannel用来与客户端之间的通信
            SocketChannel sc = ssc.accept(); // 阻塞方法，会让线程hold住
            if (sc != null) {
                log.info("connected :{}.........",sc);
                sc.configureBlocking(false); // channel.read(buffer);方法会继续运行，如果没有数据返回0
                channels.add(sc);
            }

            for (SocketChannel channel:channels) {

                // 4、接收客户端发送的数据
                int read = channel.read(buffer);// 阻塞方法，线程hold
                if(read > 0) {
                    log.info("after read.......{}", channel);
                    buffer.flip(); //切换到读模式
                    while (buffer.hasRemaining()) {
                        System.out.println((char) buffer.get());
                    }
                    buffer.clear();
                }
            }

        }

    }
}
