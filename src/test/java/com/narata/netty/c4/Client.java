package com.narata.netty.c4;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:45 2021-11-16
 * @Modified By:
 **/
public class Client {

    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();
        sc.connect(new InetSocketAddress("localhost",8080));

          System.out.println("waiting..........");
    }
}
