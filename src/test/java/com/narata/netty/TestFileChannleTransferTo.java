package com.narata.netty;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:25 2021-11-14
 * @Modified By:
 **/
public class TestFileChannleTransferTo {

    public static void main(String[] args) {

        try {
            FileChannel from  = new FileInputStream("data.txt").getChannel();
            FileChannel to = new FileOutputStream("to.txt").getChannel();

            // 效率高，底层会运用操作系统的零拷贝进行优化，2g数据
            long size = from.size();
            // left 表示剩余多少字节
            for (long left = from.size(); left >0;) {
                System.out.println("position:"+(size-left) + "left:" +left);
                left -= from.transferTo(size -left, left, to);
            }
            from.transferTo(0, from.size(),to);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
