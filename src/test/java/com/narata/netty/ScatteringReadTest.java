package com.narata.netty;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:01 2021-10-31
 * @Modified By:
 **/
public class ScatteringReadTest {

    public static void main(String[] args) {
        ByteBuffer b1 = StandardCharsets.UTF_8.encode("hello");
        ByteBuffer b2 = StandardCharsets.UTF_8.encode("world");
        ByteBuffer b3 = StandardCharsets.UTF_8.encode("你好");

        try {
            FileChannel channel = new RandomAccessFile("word2.txt","rw").getChannel();
            channel.write(new ByteBuffer[]{b1,b2,b3});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
