package com.narata.netty.n1;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.net.InetSocketAddress;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:59 2022-04-27
 * @Modified By:
 **/
public class HelloClient {

    public static void main(String[] args) throws InterruptedException {
        // 1 启动类
        new Bootstrap()
                // 2 添加EventLoop
        .group(new NioEventLoopGroup())
                // 3 选择客户端channel实现
        .channel(NioSocketChannel.class)
                // 4 添加处理器
        .handler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                nioSocketChannel.pipeline().addLast(new StringEncoder());
            }
        })
                // 5 连接到服务器
        .connect(new InetSocketAddress("localhost",8080))
                .sync()
                .channel()
                // 6 向服务器发送数据
                .writeAndFlush("hello world");
    }
}
