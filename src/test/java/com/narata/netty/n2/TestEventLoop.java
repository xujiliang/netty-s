package com.narata.netty.n2;

import io.netty.channel.DefaultEventLoop;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.NettyRuntime;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:31 2022-04-27
 * @Modified By:
 **/
@Slf4j
public class TestEventLoop {

    public static void main(String[] args) {

        // 1 创建事件循环组
        EventLoopGroup group =  new NioEventLoopGroup(2); // 可以出来io事件，普通任务，定时任务
        // EventLoopGroup group = new DefaultEventLoop(); // 可以处理普通任务，定时任务

      //  System.out.println(NettyRuntime.availableProcessors());


        // 2 获取下一个事件循环对象
//        System.out.println( group.next());
//        System.out.println( group.next());
//        System.out.println( group.next());
//        System.out.println( group.next());

        // 3 执行普通任务
        group.next().submit(() -> {
           log.info("ok");
        });

        // 4 执行定时任务
        group.next().scheduleAtFixedRate(() -> {
            log.info("haha");
        }, 1,1 , TimeUnit.SECONDS);

    }
}
