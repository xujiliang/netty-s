package com.narata.netty.n2;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:53 2022-04-27
 * @Modified By:
 **/
@Slf4j
public class EventLoopServer {

    public static void main(String[] args) {

        // (2) 创建一个独立的EventLoopGroup,处理特定的任务

        EventLoopGroup group = new DefaultEventLoopGroup();
         new ServerBootstrap()
                 // boss 和 worker
                 //（1） 1 boss只负责ServerSocketChannel上accept事件  2 worker只负责socketChannel上的读写
                 .group(new NioEventLoopGroup(), new NioEventLoopGroup(2))
                 .channel(NioServerSocketChannel.class)
                 .childHandler(new ChannelInitializer<NioSocketChannel>() {
                     @Override
                     protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                         nioSocketChannel.pipeline().addLast("handler1",new ChannelInboundHandlerAdapter() {
                             @Override
                             public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                 ByteBuf buffer = (ByteBuf) msg;
                                 log.debug(buffer.toString(Charset.defaultCharset()));
                                 ctx.fireChannelRead(msg); // 让消息传递给下一个handler
                             }
                         }).addLast(group,"handler2", new ChannelInboundHandlerAdapter() {
                             @Override
                             public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                 ByteBuf buffer = (ByteBuf) msg;
                                 log.debug(buffer.toString(Charset.defaultCharset()));
                             }
                         });
                     }
                 }).bind(8080);
    }
}
