package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

import java.util.Set;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:02 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class GroupCreateRequestMessage extends Message {

    private String groupName;
    private Set<String> members;

    public GroupCreateRequestMessage(String groupName, Set<String> members) {
        this.groupName = groupName;
        this.members = members;
    }

    @Override
    public int getMessageType() {
        return GROUP_CREATE_REQUEST_MESSAGE;
    }
}
