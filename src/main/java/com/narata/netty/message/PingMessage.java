package com.narata.netty.message;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:18 2022-05-31
 * @Modified By:
 **/
public class PingMessage extends Message{

    @Override
    public int getMessageType() {
        return 0;
    }
}
