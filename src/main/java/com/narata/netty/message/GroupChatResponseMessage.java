package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:04 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class GroupChatResponseMessage extends AbstractResponseMessage {

    private String from;
    private String content;

    public GroupChatResponseMessage(String from, String content) {
        this.from = from;
        this.content = content;
    }

    public GroupChatResponseMessage(boolean success, String reason) {
        super(success, reason);
        this.from = from;
    }

    @Override
    public int getMessageType() {
        return GROUP_CHAT_RESPONSE_MESSAGE;
    }
}
