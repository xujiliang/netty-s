package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:59 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class GroupJoinRequestMessage extends Message {

    private String groupName;

    private String username;

    public GroupJoinRequestMessage(String groupName, String username) {
        this.groupName = groupName;
        this.username = username;
    }

    @Override
    public int getMessageType() {
        return GROUP_JOIN_REQUEST_MESSAGE;
    }
}
