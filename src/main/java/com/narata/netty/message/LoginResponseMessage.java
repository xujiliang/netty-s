package com.narata.netty.message;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:44 2022-05-11
 * @Modified By:
 **/
public class LoginResponseMessage extends AbstractResponseMessage {

    public LoginResponseMessage(boolean success, String reason) {
        super(success, reason);
    }

    @Override
    public int getMessageType() {
        return LOGIN_RESPONSE_MESSAGE;
    }
}
