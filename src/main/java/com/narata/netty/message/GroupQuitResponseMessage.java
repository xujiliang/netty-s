package com.narata.netty.message;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:46 2022-05-11
 * @Modified By:
 **/
public class GroupQuitResponseMessage extends AbstractResponseMessage {

    public GroupQuitResponseMessage(boolean success, String reason) {
        super(success, reason);
    }

    @Override
    public int getMessageType() {
        return GROUP_QUIP_RESPONSE_MESSAGE;
    }
}
