package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:48 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class GroupMemberRequestMessage extends Message {

    private static final long serialVersionUID = -3114421703817205926L;

    private String groupName;

    public GroupMemberRequestMessage(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public int getMessageType() {
        return GROUP_MEMBER_REQUEST_MESSAGE;
    }
}
