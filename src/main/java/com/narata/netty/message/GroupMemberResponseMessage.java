package com.narata.netty.message;

import java.util.Set;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:49 2022-05-11
 * @Modified By:
 **/
public class GroupMemberResponseMessage extends Message {

    private Set<String> members;

    public GroupMemberResponseMessage(Set<String> members) {
        this.members = members;
    }

    @Override
    public int getMessageType() {
        return GROUP_MEMBER_RESPONSE_MESSAGE;
    }
}
