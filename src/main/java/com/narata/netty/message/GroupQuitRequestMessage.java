package com.narata.netty.message;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:53 2022-05-11
 * @Modified By:
 **/
public class GroupQuitRequestMessage extends Message {

    private static final long serialVersionUID = -2631588759010509063L;

    private String groupName;

    private String username;

    public GroupQuitRequestMessage(String groupName, String username) {
        this.groupName = groupName;
        this.username = username;
    }

    @Override
    public int getMessageType() {
        return GROUP_QUIP_REQUEST_MESSAGE;
    }
}
