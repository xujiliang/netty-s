package com.narata.netty.message;

import lombok.Getter;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:08 2022-05-11
 * @Modified By:
 **/
@Getter
public class RpcRequestMessage extends Message {
    private static final long serialVersionUID = 5105157642019019478L;

    private int sequenceId;

    /**
     * 调用接口的全限定名，服务端根据它找到实现
     */
    private String interfaceName;

    /**
     * 调用接口中的方法
     */
    private String methodName;

    /**
     * 方法的返回类型
     */
    private Class<?> treureType;

    /**
     * 方法参数类型数据
     */
    private Class[] parameterType;

    /**
     * 方法参数数组
     */
    private Object[] parameterValue;


    public RpcRequestMessage(int sequenceId, String interfaceName, String methodName, Class<?> treureType, Class[] parameterType, Object[] parameterValue) {
        this.sequenceId = sequenceId;
        this.interfaceName = interfaceName;
        this.methodName = methodName;
        this.treureType = treureType;
        this.parameterType = parameterType;
        this.parameterValue = parameterValue;
    }

    @Override
    public int getMessageType() {
        return RPC_MESSAGE_TYPE_REQUEST;
    }
}
