package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:46 2022-05-10
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class LoginRequestMessage extends Message {


    private static final long serialVersionUID = 2790221244514765382L;

    private String username;

    private String password;

    public LoginRequestMessage(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public LoginRequestMessage() {
    }

    @Override
    public int getMessageType() {
        return LOGIN_REQUEST_MESSAGE;
    }
}
