package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:55 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class GroupJoinResponseMessage extends AbstractResponseMessage {

    public GroupJoinResponseMessage(boolean success, String reason) {
        super(success, reason);
    }

    @Override
    public int getMessageType() {
        return GROUP_JOIN_RESPONSE_MESSAGE;
    }
}
