package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:43 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public abstract class AbstractResponseMessage extends Message {

    private static final long serialVersionUID = 1200412702372465402L;

    private boolean success;

    private String reason;

    public AbstractResponseMessage() {
    }

    public AbstractResponseMessage(boolean success, String reason) {
        this.success = success;
        this.reason = reason;
    }
}
