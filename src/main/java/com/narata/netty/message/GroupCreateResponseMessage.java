package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 21:01 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class GroupCreateResponseMessage extends AbstractResponseMessage {

    public GroupCreateResponseMessage() {
    }

    public GroupCreateResponseMessage(boolean success, String reason) {
        super(success, reason);
    }

    @Override
    public int getMessageType() {
        return GROUP_CREATE_RESPONSE_MESSAGE;
    }
}
