package com.narata.netty.message;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 20:45 2022-05-11
 * @Modified By:
 **/
@Data
@ToString(callSuper = true)
public class GroupQuitMessage extends Message {

    private static final long serialVersionUID = 4499484532668375786L;

    private String groupName;

    private String username;

    @Override
    public int getMessageType() {
        return GROUP_QUIP_REQUEST_MESSAGE;
    }
}
