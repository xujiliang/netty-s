package com.narata.netty.chat;

import com.narata.netty.message.*;
import com.narata.netty.protocol.MessageCodecSharable;
import com.narata.netty.protocol.ProcotolFrameDecoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:13 2022-05-16
 * @Modified By:
 **/
@Slf4j
public class ChatClient {

    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler();
        MessageCodecSharable MESSAGE_CODEC = new MessageCodecSharable();

        CountDownLatch WAIT_FOR_LOGIN =  new CountDownLatch(1);
        AtomicBoolean LOGIN = new AtomicBoolean(false);
        try {

            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {

                    ch.pipeline().addLast(new ProcotolFrameDecoder());
                    ch.pipeline().addLast(LOGGING_HANDLER);
                    ch.pipeline().addLast(MESSAGE_CODEC);

                    // 用来判断是不是读空闲时间过长或者写时间过长
                    // 5s内如果没有收到channel的数据，会触发一个IdleState#READER_IDLE事件
                    ch.pipeline().addLast(new IdleStateHandler(0,3,0));
                    // 同时作为入栈和出栈处理器
                    ch.pipeline().addLast(new ChannelDuplexHandler() {
                        // 用来触发特殊事件
                        @Override
                        public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
                            IdleStateEvent event = (IdleStateEvent) evt;
                            // 触发读空闲事件
                            if (event.state() == IdleState.WRITER_IDLE) {
                                log.debug("beat beat");
                                ctx.writeAndFlush(new PingMessage());
                            }

                        }
                    });

                    ch.pipeline().addLast("Client handler",new ChannelInboundHandlerAdapter() {

                        @Override
                        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

                            if (msg instanceof LoginResponseMessage) {
                                LoginResponseMessage message = (LoginResponseMessage) msg;
                                if (message.isSuccess()) {
                                    // 如果登陆成功
                                    LOGIN.set(true);
                                } else {
                                    log.debug("登陆失败");
                                }
                                WAIT_FOR_LOGIN.countDown();
                            }
                            // 唤醒线程
                            log.debug("----------msg:{}", msg);
                        }

                        // 在连接建立后触发active事件
                        @Override
                        public void channelActive(ChannelHandlerContext ctx) throws Exception {

                            // 负责接收用户在控制台的输入，负责向服务器发送数据
                            new Thread(() -> {
                                Scanner scanner = new Scanner(System.in);
                                System.out.println("请输入用户名：");
                                String userName = scanner.nextLine();
                                System.out.println("请输入密码：");
                                String password = scanner.nextLine();

                                LoginRequestMessage loginRequestMessage = new LoginRequestMessage(userName, password);
                                ctx.writeAndFlush(loginRequestMessage);

                                System.out.println("等待后续操作");
                                try {
                                    WAIT_FOR_LOGIN.await();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                // 如果登陆失败
                                if (!LOGIN.get()) {
                                    ctx.channel().close();
                                    return;
                                }
                                while (true) {
                                    System.out.println("=============================================");

                                    System.out.println("send [username] [content]");
                                    System.out.println("gsend [group name] [content]");
                                    System.out.println("gcreate [group name] [m1,m2,m3...]");
                                    System.out.println("gmembers [group name]");
                                    System.out.println("gjoin [group name]");
                                    System.out.println("gquit [group name]");
                                    System.out.println("quit");
                                    System.out.println("=============================================");

                                    String command = scanner.nextLine();
                                    String[] s = command.split(" ");
                                    log.debug("command:{}", s);
                                    switch (s[0]) {
                                        case "send":
                                            ctx.writeAndFlush(new ChatRequestMessage(userName ,s[1], s[2]));
                                            break;
                                        case "gsend":
                                            ctx.writeAndFlush(new GroupChatRequestMessage(userName,s[1], s[2]));
                                            break;
                                        case "gcreate":
                                            Set<String> set = new HashSet<>( Arrays.asList(s[2].split(",")));
                                            set.add(userName); // 加入自己
                                            ctx.writeAndFlush(new GroupCreateRequestMessage(s[1],set));
                                            break;
                                        case "gmembers":
                                            ctx.writeAndFlush(new GroupMemberRequestMessage(s[1]));
                                            break;
                                        case "gjoin":
                                            ctx.writeAndFlush(new GroupJoinRequestMessage(userName,s[1]));
                                            break;
                                        case "gquit":
                                            ctx.writeAndFlush(new GroupQuitRequestMessage(s[1], s[2]));
                                            break;
                                        case "quit":
                                            ctx.channel().close();
                                            break;
                                    }

                                }
                            },"system in").start();
                        }
                    });
                }
            });

            Channel channel = bootstrap.connect("localhost", 8080).sync().channel();
            channel.closeFuture().sync();
        } catch (Exception e) {
            log.debug("error:{}", e);
        } finally {
            group.shutdownGracefully();
        }

    }
}
