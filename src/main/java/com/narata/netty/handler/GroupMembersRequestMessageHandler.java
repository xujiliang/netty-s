package com.narata.netty.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:31 2022-05-29
 * @Modified By:
 **/
@ChannelHandler.Sharable
public class GroupMembersRequestMessageHandler extends SimpleChannelInboundHandler<GroupMembersRequestMessageHandler> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupMembersRequestMessageHandler msg) throws Exception {

    }
}
