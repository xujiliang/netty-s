package com.narata.netty.handler;

import com.narata.netty.message.GroupChatRequestMessage;
import com.narata.netty.message.GroupChatResponseMessage;
import com.narata.netty.session.GroupSessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:31 2022-05-29
 * @Modified By:
 **/
@ChannelHandler.Sharable
public class GroupChatRequestMessageHandler extends SimpleChannelInboundHandler<GroupChatRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupChatRequestMessage msg) throws Exception {

        String groupName = msg.getGroupName();
        String content = msg.getContent();
        String from = msg.getFrom();
        List<Channel> membersChannel = GroupSessionFactory.getGroupSession().getMembersChannel(groupName);
        for (Channel channel : membersChannel) {
            channel.writeAndFlush(new GroupChatResponseMessage(from, content));
        }
    }
}
