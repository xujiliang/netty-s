package com.narata.netty.service;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 22:20 2022-05-16
 * @Modified By:
 **/
public interface UserService {
    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     * @return 登录成功返回 true, 否则返回 false
     */
    boolean login(String username, String password);
}
