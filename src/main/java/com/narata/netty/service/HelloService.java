package com.narata.netty.service;

public interface HelloService {
    String sayHello(String name);
}
