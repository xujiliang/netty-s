package com.narata.netty.protocol;

import com.narata.netty.message.LoginRequestMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LoggingHandler;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:45 2022-05-10
 * @Modified By:
 **/
public class TestMessageCodec {

    public static void main(String[] args) throws Exception {

        EmbeddedChannel channel = new EmbeddedChannel(
                new LoggingHandler(),
                new LengthFieldBasedFrameDecoder(1024,12,4,0,0),
                new MessageCodec());

        // encode
        LoginRequestMessage requestMessage = new LoginRequestMessage("zhansan", "123");
        channel.writeOutbound(requestMessage);

        // decode
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer();

        new MessageCodec().encode(null, requestMessage, buffer);

        ByteBuf slice1 = buffer.slice(0, 100);
        ByteBuf slice2 = buffer.slice(100, buffer.readableBytes() - 100);
        slice1.retain();
        channel.writeInbound(slice1);
        channel.writeInbound(slice2);

    }
}
