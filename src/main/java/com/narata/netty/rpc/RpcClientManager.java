package com.narata.netty.rpc;

import com.narata.netty.message.RpcRequestMessage;
import com.narata.netty.protocol.MessageCodecSharable;
import com.narata.netty.protocol.ProcotolFrameDecoder;
import com.narata.netty.protocol.SequenceIdGenerator;
import com.narata.netty.service.HelloService;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.DefaultPromise;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Proxy;

/**
 * @Author: XJL
 * @Description:
 * @Date: Create in 23:28 2022-07-03
 * @Modified By:
 **/
@Slf4j
public class RpcClientManager {

    public static Channel channel = null;

    public static void main(String[] args) {
        HelloService service = getProxyService(HelloService.class);
        String zhansan = service.sayHello("zhansan");
        System.out.println("===============:" + zhansan);
    }


    public static <T> T getProxyService(Class<T> serviceClass) {

        ClassLoader loader = serviceClass.getClassLoader();

        Class[] interfaces = {serviceClass};

        Object o = Proxy.newProxyInstance(loader, interfaces, (proxy, method, args) -> {
            // 1. 将方法调用转换为 消息对象
            int sequenceId = SequenceIdGenerator.nextId();
            RpcRequestMessage requestMessage = new RpcRequestMessage(sequenceId, serviceClass.getName(), method.getName(), method.getReturnType(), method.getParameterTypes(), args);
            // 2 将消息发送出去
            getChannel().writeAndFlush(requestMessage);

            // 3. 准备一个空Promise对象，来接收结果 ，指定promise对象异步接收结果线程
            DefaultPromise<Object> promise = new DefaultPromise<>(getChannel().eventLoop());
            RpcResponseMessageHandler.PROMISES.put(sequenceId, promise);

            // 4 等待promise结果
            promise.await();
            if (promise.isSuccess()) {
                // 调用正常
                return promise.getNow();
            } else {
                // 调用失败
                throw new RuntimeException(promise.cause());
            }
        });

        return (T) o;
    }

    public static Channel getChannel() {
        if (channel != null) {
            return channel;
        }
        synchronized (RpcClientManager.class) {
            if (channel != null) {
                return channel;
            }
            initChannel();
            return channel;
        }
    }

    // 初始化channel方法
    private static void initChannel() {
        NioEventLoopGroup group = new NioEventLoopGroup();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler(LogLevel.DEBUG);
        MessageCodecSharable MESSAGE_CODEC = new MessageCodecSharable();

        RpcResponseMessageHandler RPC_HANDLER = new RpcResponseMessageHandler();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.group(group);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) {
                ch.pipeline().addLast(new ProcotolFrameDecoder());
                ch.pipeline().addLast(LOGGING_HANDLER);
                ch.pipeline().addLast(MESSAGE_CODEC);
                ch.pipeline().addLast(RPC_HANDLER);
            }
        });
        try {
            channel = bootstrap.connect("localhost", 9090).sync().channel();
            channel.closeFuture().addListener(future -> {
                log.info("shutdownGracefully.....................");
                group.shutdownGracefully();
            });
        } catch (Exception e) {
            log.error("client error:", e);
        }
    }
}
