# NIO基础 
non-blocking io 非阻塞IO
## 1、三大组件 

### 1.1 Channel & Buffer

` channel有一点类似于stream，它就是读写数据的双向通道，可以从channel将数据读物buffer，也可以将buffer的数据写入channel，而之前的stream
 要么是输出，要么输输入，channel比stream更为底层。`
    
                         ------->     
                channel            buffer
                         <-------                       


常见的Channel有：
``` 
FileChannel
DatagramChannel
SocketChanel
ServerSocketChannel

```


常用的buffer則用來缓冲读写数据，常见的buffer有：
ByteBuffer

### 1.3 Selector
